//
//  DetailsVC.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 06/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var enterpriseImage: UIImageView!
    @IBOutlet var enterpriseDescription: UITextView!
        
    var enterprise: Enterprise?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterpriseDescription.delegate = self
        self.enterpriseImage.image = bigLogo()
        self.name.text = self.enterprise?.name
        self.enterpriseDescription.text = self.enterprise?.description
    }
    
    func initEnterprise(enterprise: Enterprise) {
        self.enterprise = enterprise
    }
    
    // generate other big to optimization
    func bigLogo() -> UIImage {
        let color = UIColor(cgColor: LOGO_COLORS_ENTERPRISE_TYPE[(enterprise?.category?.id)!]!)
        return UIImage.createLogo(enterpriseName: (enterprise?.name!)!, logoSize: CGSize(width: 375, height: 184), logoColor: color, logoFontSize: 130)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
