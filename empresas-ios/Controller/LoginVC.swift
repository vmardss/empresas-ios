//
//  LoginVC.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 04/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.spinner.isHidden = true
        self.email.delegate = self
        self.password.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if AuthService.instance.logged {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "EnterprisesVC")
            self.present(viewController!, animated: false)
        }
    }
    
    @IBAction func logginBtnPressed(_ sender: Any) {
        if !isEmailOrPasswordBlank() {
            if isValidEmail(email: self.email.text!) {
                spinner.isHidden = false
                spinner.startAnimating()
                
                AuthService.instance.login(email: self.email.text!, password: self.password.text!) { (success) in
                    if success {
                        self.spinner.isHidden = true
                        self.spinner.stopAnimating()
                        self.performSegue(withIdentifier: "toSearch", sender: self)
                    } else {
                        self.spinner.isHidden = true
                        self.spinner.stopAnimating()
                        self.startAleart(msg: AuthService.instance.msgReqError)
                    }
                }
            } else {
                self.startAleart(msg: MSG_EMAIL_ERROR)
            }
        } else {
            self.startAleart(msg: MSG_EMAIL_OR_PASSWORD_BLANK_ERROR)
        }
    }
    
    func startAleart(msg: String) {
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        AuthService.instance.msgReqError = MSG_REQ_ERROR
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    func isEmailOrPasswordBlank() -> Bool {
        var blank = true
        if self.email.text != "" && self.password.text != "" {
             blank = false
        }
        return blank
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        email.resignFirstResponder()
        password.resignFirstResponder()
        return true
    }
}
