//
//  SearchVC.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 04/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import UIKit

class EnterprisesVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var enterpriseTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var toSearchBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterpriseTable.dataSource = self
        enterpriseTable.delegate = self
        searchBar.delegate = self
        searchBar.backgroundImage = UIImage() // fix search bar top and botton border
        enterpriseTable.isHidden = true
        changeSearchCancelButtonColorAndSearchCleanIcon()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        letTheSearchBarCancelAlwaysEnabled()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EnterpriseService.instance.enterprises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EnterpriseCell") as? EnterpriseCell  {
            let enterprise = EnterpriseService.instance.enterprises[indexPath.row]
            cell.updateCell(enterprise: enterprise)
            return cell;
        } else {
            return EnterpriseCell();
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let enterprise = EnterpriseService.instance.enterprises[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "toDetails", sender: enterprise)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsVC = segue.destination as? DetailsVC {
            detailsVC.initEnterprise(enterprise: sender as! Enterprise)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        requestData(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        letTheSearchBarCancelAlwaysEnabled()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        enterpriseTable.isHidden = true
        searchBar.isHidden = true
        toSearchBtn.isHidden = false
        searchBar.text = ""
    }
    
    @IBAction func toSearchBtnPressed(_ sender: Any) {
        if enterpriseTable.isHidden {
            enterpriseTable.isHidden = false
        }
        requestData(searchText: "")
        searchBar.isHidden = false
        toSearchBtn.isHidden = true
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func toSettingsBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: "toSettings", sender: nil)
    }
    
    func requestData(searchText: String) {
        if FiltersServices.instance.category == "Nenhuma" {
            EnterpriseService.instance.searchByName(name: searchText) { (success) in
                if success {
                    self.enterpriseTable.reloadData()
                } else {
                    self.startAleart()
                }
            }
        } else {
            EnterpriseService.instance.searchByNameAndEnterpise(name: searchText) { (success) in
                if success {
                    self.enterpriseTable.reloadData()
                } else {
                    self.startAleart()
                }
            }
        }
        enterpriseTable.reloadData()
    }
    
    func letTheSearchBarCancelAlwaysEnabled() {
        for view in searchBar.subviews {
            for subview in view.subviews {
                if let button = subview as? UIButton {
                    button.isEnabled = true
                }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func startAleart() {
        let alert = UIAlertController(title: "Error", message: MSG_REQ_ERROR, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func changeSearchCancelButtonColorAndSearchCleanIcon() {
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        // I couldn't find any better icon to replace D:
        searchBar.setImage(UIImage(named: "clean"), for: .clear, state: .normal)
    }
}
