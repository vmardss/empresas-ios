//
//  SettingsVC.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 09/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var userText: UILabel!
    @IBOutlet var categoryBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.isHidden = true
        userText.text = AuthService.instance.uid
        categoryBtn.setTitle(FiltersServices.instance.category, for: .normal)
        
    }
    
    @IBAction func categoryBtnPressed(_ sender: Any) {
        pickerView.selectRow(ENTERPRISE_TYPES.index(of: FiltersServices.instance.category)!, inComponent: 0, animated: false)
        pickerView.isHidden = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ENTERPRISE_TYPES[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ENTERPRISE_TYPES.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        categoryBtn.setTitle(ENTERPRISE_TYPES[row], for: .normal)
    }
    
    @IBAction func saveBtnPressed(_ sender: Any) {
        FiltersServices.instance.category = categoryBtn.currentTitle!
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loggoutBtnPressed(_ sender: Any) {
        AuthService.instance.logout()
        performSegue(withIdentifier: "toLogout", sender: nil)
    }
}
