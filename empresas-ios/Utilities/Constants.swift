//
//  Constants.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 04/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import Foundation
import UIKit

let URL_BASE = "http://54.94.179.135:8090/api/v1/"
let URL_LOGIN = "\(URL_BASE)/users/auth/sign_in"
let URL_GET_ENTERPRISES_NAME = "\(URL_BASE)/enterprises?name="
let URL_GET_ENTERPRISES_TYPE = "\(URL_BASE)/enterprises?enterprise_types="
let URL_GET_ENTERPRISE_NAME_APPEND="&name="
let LOGGED_KEY = "logged"
let TOKEN_KEY = "access-token"
let CLIENT_KEY = "client"
let UID_KEY = "uid"
let FILTER_KEY = "Nenhuma"

let MSG_REQ_ERROR = "Unknown error."
let MSG_EMAIL_ERROR = "Email Address in invalid format."
let MSG_EMAIL_OR_PASSWORD_BLANK_ERROR = "Email or Password cannot be blank."

let BASIC_HEADER = ["Content-Type": "application/json; charset=utf-8"]
let ACCESS_HEADERS = ["Content-Type": "application/json; charset=utf-8",
                      "access-token": AuthService.instance.accessToken,
                      "client": AuthService.instance.client,
                      "uid": AuthService.instance.uid]

typealias CompletionHandler = (_ Success: Bool) -> ()

let LOGO_COLORS_ENTERPRISE_TYPE: [Int : CGColor] = [1 : #colorLiteral(red: 1, green: 0.4941176471, blue: 0.4745098039, alpha: 1).cgColor, 2 : #colorLiteral(red: 1, green: 0.8323456645, blue: 0.4732058644, alpha: 1).cgColor, 3 : #colorLiteral(red: 0.9995340705, green: 0.988355577, blue: 0.4726552367, alpha: 1).cgColor, 4 : #colorLiteral(red: 0.8321695924, green: 0.985483706, blue: 0.4733308554, alpha: 1).cgColor,  5 : #colorLiteral(red: 0.4500938654, green: 0.9813225865, blue: 0.4743030667, alpha: 1).cgColor, 6 : #colorLiteral(red: 0.4508578777, green: 0.9882974029, blue: 0.8376303315, alpha: 1).cgColor, 7 : #colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1).cgColor, 8 : #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1).cgColor, 9 : #colorLiteral(red: 0.476841867, green: 0.5048075914, blue: 1, alpha: 1).cgColor, 10 : #colorLiteral(red: 0.8446564078, green: 0.5145705342, blue: 1, alpha: 1).cgColor, 11 : #colorLiteral(red: 1, green: 0.5409764051, blue: 0.8473142982, alpha: 1).cgColor, 12 : #colorLiteral(red: 0.5807225108, green: 0.066734083, blue: 0, alpha: 1).cgColor, 13 : #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1).cgColor, 14 : #colorLiteral(red: 0.5738074183, green: 0.5655357838, blue: 0, alpha: 1).cgColor, 15 : #colorLiteral(red: 0.3084011078, green: 0.5618229508, blue: 0, alpha: 1).cgColor, 16 : #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1).cgColor, 17 : #colorLiteral(red: 0, green: 0.5690457821, blue: 0.5746168494, alpha: 1).cgColor, 18 : #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1).cgColor, 19 : #colorLiteral(red: 0, green: 0.3285208941, blue: 0.5748849511, alpha: 1).cgColor, 20 : #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1).cgColor, 21 : #colorLiteral(red: 0.3236978054, green: 0.1063579395, blue: 0.574860394, alpha: 1).cgColor, 22 : #colorLiteral(red: 0.5810584426, green: 0.1285524964, blue: 0.5745313764, alpha: 1).cgColor, 23 : #colorLiteral(red: 0.5808190107, green: 0.0884276256, blue: 0.3186392188, alpha: 1).cgColor, 24 : #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1).cgColor]

let ENTERPRISE_TYPE_INDEX: [String : Int?] = ["Nenhuma" : nil, "Agro" : 1, "Aviation" : 2, "Biotech" : 3, "Eco" : 4,  "Ecommerce" : 5 , "Education" : 6, "Fashion" : 7, "Fintech" : 8, "Food" : 9, "Games" : 10, "Health" : 11, "IOT" : 12, "Logistics" : 13, "Media" : 14, "Mining" : 15, "Products" : 16, "Real Estate" : 17, "Service" : 18, "Smart City" : 19, "Social" : 20, "Software" : 21, "Technology" : 22, "Tourism" : 23, "Transport" : 24]

let ENTERPRISE_TYPES = ["Nenhuma", "Agro", "Aviation", "Biotech", "Eco", "Ecommerce", "Education", "Fashion", "Fintech", "Food", "Games", "Health", "IOT", "Logistics", "Media", "Mining", "Products", "Real Estate", "Service", "Smart City", "Social", "Software", "Technology", "Tourism", "Transport"]
