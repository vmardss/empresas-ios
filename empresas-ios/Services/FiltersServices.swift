//
//  FiltersServices.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 09/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import Foundation

class FiltersServices {
    
    static let instance = FiltersServices()
    
    let defaults = UserDefaults.standard
    
    var category: String {
        get {
            if defaults.value(forKey: FILTER_KEY) == nil {
                self.category = "Nenhuma"
            }
            return defaults.value(forKey: FILTER_KEY) as! String
        }
        set {
            return defaults.set(newValue, forKey: FILTER_KEY)
        }
    }
}
