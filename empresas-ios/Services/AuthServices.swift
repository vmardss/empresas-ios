//
//  AuthServices.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 04/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService {
    
    static let instance = AuthService()
    
    let defaults = UserDefaults.standard
    
    var logged: Bool {
        get {
            return defaults.bool(forKey: LOGGED_KEY)
        }
        set {
            defaults.set(newValue, forKey: LOGGED_KEY)
        }
    }
    
    var accessToken: String {
        get {
            return defaults.value(forKey: TOKEN_KEY) as! String
        }
        set {
            return defaults.set(newValue, forKey: TOKEN_KEY)
        }
    }
    
    var client: String {
        get {
            return defaults.value(forKey: CLIENT_KEY) as! String
        }
        set {
            return defaults.set(newValue, forKey: CLIENT_KEY)
        }
    }
    
    var uid: String {
        get {
            return defaults.value(forKey: UID_KEY) as! String
        }
        set {
            return defaults.set(newValue, forKey: UID_KEY)
        }
    }
    
    var msgReqError = MSG_REQ_ERROR
    
    func login(email:  String, password: String, completion: @escaping CompletionHandler) {
        let body: [String: Any] = [
            "email" : email.lowercased(),
            "password" : password
        ]
       
        Alamofire.request(URL_LOGIN, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BASIC_HEADER).responseJSON {
            (response) in
            
            let headers = response.response?.allHeaderFields
            if headers != nil && headers!["Status"] != nil {
                if headers!["Status"] as! String == "200 OK" {
                    self.accessToken = headers!["access-token"] as! String
                    self.client = headers!["client"] as! String
                    self.uid = headers!["uid"] as! String
                    self.logged = true
                    completion(true)
                } else {
                    let json = try! JSON(data: response.data!)
                    self.msgReqError = json["errors"][0].stringValue
                    completion(false)
                }
            } else {
                completion(false)
            }
        }
    }
    
    func logout() {
        self.logged = false
        self.accessToken = ""
        self.client = ""
        self.uid = ""
        FiltersServices.instance.category = "Nenhuma"
    }
}
