//
//  EnterpriseServices.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 05/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper

class EnterpriseService {
    
    static let instance = EnterpriseService()
    
    var enterprises = [Enterprise]()
    
    func searchByName(name: String, completion: @escaping CompletionHandler) {
        let nameFilter = (URL_GET_ENTERPRISES_NAME + name).replacingOccurrences(of: " ", with: "%20")
        
        Alamofire.request(nameFilter, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ACCESS_HEADERS).responseJSON { (response) in
            
            if response.result.error == nil {
                let data = response.data
                let json = try! JSON(data: data!)
                let enterprises = json["enterprises"].arrayValue
                self.cleanEnterprises()
                for enterprise in enterprises {
                    let ent = Enterprise(JSONString: enterprise.rawString()!)
                    self.enterprises.append(ent!)
                }
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
    
    func searchByNameAndEnterpise(name: String, completion: @escaping CompletionHandler) {
        let enterpriseIndex = ENTERPRISE_TYPE_INDEX[FiltersServices.instance.category]!
        let nameFilter = (URL_GET_ENTERPRISES_TYPE + String(enterpriseIndex!) + URL_GET_ENTERPRISE_NAME_APPEND + name).replacingOccurrences(of: " ", with: "%20")
        
        Alamofire.request(nameFilter, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ACCESS_HEADERS).responseJSON { (response) in
            
            if response.result.error == nil {
                let data = response.data
                let json = try! JSON(data: data!)
                let enterprises = json["enterprises"].arrayValue
                self.cleanEnterprises()
                for enterprise in enterprises {
                    let ent = Enterprise(JSONString: enterprise.rawString()!)
                    self.enterprises.append(ent!)
                }
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
    
    func cleanEnterprises() {
        self.enterprises = [Enterprise]()
    }
}
