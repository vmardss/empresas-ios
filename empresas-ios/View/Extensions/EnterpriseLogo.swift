//
//  EnterpriseLogo.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 08/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    static func createLogo(enterpriseName: String, logoSize: CGSize, logoColor: UIColor, logoFontSize: CGFloat) -> UIImage {
        let imageGenerated = UIImage(color: logoColor, size: logoSize)!
        let font = UIFont(name: "Helvetica-Bold", size: logoFontSize)
        var abbreviation = ""
        if enterpriseName.contains(" ") {
            let wordSplited = enterpriseName.split(separator: " ")
            let firstWord = wordSplited[0]
            let secondWord = wordSplited[wordSplited.count - 1]
            let indexEndFirstWord = firstWord.index(firstWord.endIndex, offsetBy: -(firstWord.count - 1))
            let indexEndSecondWord = secondWord.index(secondWord.endIndex, offsetBy: -(secondWord.count - 1))
            abbreviation = firstWord[..<indexEndFirstWord] + "" + secondWord[..<indexEndSecondWord]
        } else {
            let index = enterpriseName.index(enterpriseName.endIndex, offsetBy: -(enterpriseName.count - 2))
            abbreviation = String(enterpriseName[..<index])
        }
        return imageGenerated.centerText(abbreviation: abbreviation.uppercased(), font: font!)
    }
    
    convenience init?(color: UIColor, size: CGSize) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    private func centerText(abbreviation: String, font: UIFont) -> UIImage {
        let textFontAttributes = [
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),
            ] as [NSAttributedStringKey : Any]
        let abbreviationSize = NSString(string: abbreviation).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: textFontAttributes, context: nil)
        let x = size.width / 2 - abbreviationSize.width / 2
        let y = size.height / 2 - abbreviationSize.height / 2
        UIGraphicsBeginImageContext(size)
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let rect = CGRect(x: x, y: y, width: size.width, height: size.height)
        abbreviation.draw(in: rect, withAttributes: textFontAttributes)
        let imageWithFont = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithFont!
    }
}
