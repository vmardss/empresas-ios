//
//  RoundButton.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 04/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import UIKit

class RoundButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 6
    }
}
