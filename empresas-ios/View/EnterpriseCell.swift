//
//  EnterpriseCell.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 05/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import UIKit

class EnterpriseCell: UITableViewCell {
    
    @IBOutlet weak var enterpriseImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var country: UILabel!
    
    func updateCell(enterprise: Enterprise) {
        enterpriseImage.image = enterprise.logo
        name.text = enterprise.name
        category.text = enterprise.category?.name
        country.text = enterprise.country
    }
}
