//
//  Category.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 08/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import Foundation

struct Category {
    var id: Int?
    var name: String?
}
