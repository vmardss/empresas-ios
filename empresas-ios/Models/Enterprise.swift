//
//  Enterprise.swift
//  empresas-ios
//
//  Created by Vinícius Santos on 05/01/18.
//  Copyright © 2018 Vinícius Santos. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class Enterprise: Mappable {
    
    var logo: UIImage?
    var id: Int?
    var name: String?
    var description: String?
    var city: String?
    var country: String?
    var category: Category?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["enterprise_name"]
        description <- map["description"]
        city <- map["city"]
        country <- map["country"]
        
        var category: [String: Any] = [:]
        category <- map["enterprise_type"]
        setCategory(category: category)
        
        createLogo()
    }
    
    func setCategory(category: [String: Any]) {
        if !category.isEmpty {
            self.category = Category()
            self.category?.id = category["id"] as? Int
            self.category?.name = category["enterprise_type_name"] as? String
        }
    }
    
    func createLogo() {
        let color = UIColor(cgColor: LOGO_COLORS_ENTERPRISE_TYPE[(self.category?.id)!]!)
        self.logo = UIImage.createLogo(enterpriseName: self.name!, logoSize: CGSize(width: 60, height: 39), logoColor: color, logoFontSize: 33)
    }
}
